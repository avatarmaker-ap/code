package com.avatarmaker.app.converter;

import com.avatarmaker.app.converter.NumberException;

public class Number {
    private static final int MIN_BASE_LIMIT = 1;
    private static final int MAX_BASE_LIMIT = 36;
    private static final String[] DIGITS = {
        "0", "1", "2", "3", "4", "5",
        "6", "7", "8", "9", "a", "b",
        "c", "d", "e", "f", "g", "h",
        "i", "j", "k", "l", "m", "n",
        "o", "p", "q", "r", "s", "t",
        "u", "v", "w", "x", "y", "z",
    };

    public static String convertTo(String number, int initBase, int convertedBase) throws NumberException {
        validateBase(initBase);
        validateBase(convertedBase);
        String[] digits = number.toLowerCase().split("");
        long intNumber = 0;
        for (int index = 0; index < digits.length; index++) {
            int digit = decimal(digits[index]);
            intNumber += digit * Math.pow(initBase,  (digits.length - (index + 1)));
        }

        if (intNumber == 0) {
            return "0";
        }

        String stringNumber = "";
        while (intNumber != 0) {
            int digit = (int) (intNumber % convertedBase);
            intNumber = intNumber / convertedBase;
            stringNumber = DIGITS[digit] + stringNumber;
        }

        return stringNumber;
    }

    private static void validateBase(int base) throws NumberException {
        if (!(MIN_BASE_LIMIT <= base && base <= MAX_BASE_LIMIT)) {
            throw new NumberException(String.format("Range base between %s until %s (Inclusive)", MIN_BASE_LIMIT, MAX_BASE_LIMIT));
        }
    }

    private static int decimal(String number) {
        int numberChar = number.charAt(0);
        if (48 <= numberChar && numberChar <= 57) {
            return numberChar - 48;
        } else {
            return numberChar - 87;
        }
    }
}