package com.avatarmaker.app.converter;
import java.lang.RuntimeException;

public class NumberException extends RuntimeException {
    public NumberException(String errorMessage) {
        super(errorMessage);
    }
}