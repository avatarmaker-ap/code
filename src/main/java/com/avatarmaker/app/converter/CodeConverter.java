package com.avatarmaker.app.converter;

import com.avatarmaker.app.converter.Number;
import java.util.List;
import java.util.ArrayList;

public class CodeConverter {
    private static final int ENCODED_BASE = 10;
    private static final int DECODED_BASE = 36;
    private static final int MAX_DIGIT_LENGTH = 2;

    public static List<String> decode(String code) {   
        String[] rawCodes = new String[code.length() / MAX_DIGIT_LENGTH];
        int rawCodeIndex = 0;
        for (int codeIndex = 0; codeIndex < code.length(); codeIndex += MAX_DIGIT_LENGTH) {
            rawCodes[rawCodeIndex++] = code.substring(codeIndex, codeIndex + MAX_DIGIT_LENGTH);
        }

        List<String> decodedCodes = new ArrayList<String>();
        for (int index = 0; index < rawCodes.length; index++) {
            decodedCodes.add(Number.convertTo(rawCodes[index], DECODED_BASE, ENCODED_BASE));        
        }
        
        return decodedCodes;
    }

    public static String encode(List<String> codes) {
        String encodedCode = "";
        for (int index = 0; index < codes.size(); index++) {
            encodedCode += setDigit(Number.convertTo(codes.get(index), ENCODED_BASE, DECODED_BASE));        
        }

        return encodedCode;
    }

    private static String setDigit(String digit) {
        while (digit.length() < MAX_DIGIT_LENGTH) {
            digit = 0 + digit;
        }
        return digit;
    }
}