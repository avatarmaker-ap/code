package com.avatarmaker.app.CodeLoader;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;



public abstract class CodeHandler{
    private CodeHandler nextHandler;

    public CodeHandler(CodeHandler nextHandler){
        this.nextHandler = nextHandler;
    }

    public abstract void handle(String code) throws CodeHandlerException;

    public CodeHandler getNextHandler() {
        return nextHandler;
    }
}