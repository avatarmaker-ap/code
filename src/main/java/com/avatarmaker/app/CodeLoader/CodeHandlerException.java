package com.avatarmaker.app.CodeLoader;

public class CodeHandlerException extends Exception{
    CodeHandlerException(String s) {
        super(s);
    }
}