package com.avatarmaker.app.CodeLoader;

import com.avatarmaker.app.CodeLoader.SymbolValidation;
import com.avatarmaker.app.CodeLoader.CodeHandler;
import com.avatarmaker.app.CodeLoader.LengthValidation;
import com.avatarmaker.app.CodeLoader.Decoder;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;



public class CodeHandlerService{
    private CodeHandler decoder;
    private CodeHandler lengthValidation;
    private CodeHandler symbolValidation;
    public CodeHandlerService(){
        this.decoder = new Decoder();
        this.symbolValidation = new SymbolValidation(decoder);
        this.lengthValidation = new LengthValidation(symbolValidation);
    }

    public void handle(String code) throws CodeHandlerException{
        try {
            lengthValidation.handle(code);
        }
        catch (CodeHandlerException e){
            throw new CodeHandlerException(e.getMessage());
        }
    }
}