package com.avatarmaker.app.CodeLoader;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;


public class LengthValidation extends CodeHandler{

    public LengthValidation(CodeHandler nextHandler){
        super(nextHandler);
    }

    private boolean verifyLength(String code){
        //TODO menambahkan kode verifikasi sesuai ciri ciri kode
        if (code.length() == 8){
            return true;
        }
        return false;
    }

    public void handle(String code) throws CodeHandlerException{
        if (verifyLength(code)) {
            getNextHandler().handle(code);
        } else {
            //TODO Mengganti ini agar mendisplay di UInya bahwa kode tidak valid.
            throw new CodeHandlerException("Invalid length");
        }
    }


}