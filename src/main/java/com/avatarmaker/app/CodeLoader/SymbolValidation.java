package com.avatarmaker.app.CodeLoader;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;

public class SymbolValidation extends CodeHandler{

    public SymbolValidation(CodeHandler nextHandler){
        super(nextHandler);
    }

    private boolean verifySymbol(String code){
        //TODO menambahkan kode verifikasi sesuai ciri ciri kode
        return code.matches("[A-Za-z0-9]+");
    }

    public void handle(String code) throws CodeHandlerException{
        if (verifySymbol(code)) {
            getNextHandler().handle(code);
        } else {
            //TODO Mengganti ini agar mendisplay di UInya bahwa kode tidak valid.
            throw new CodeHandlerException("Invalid symbol");
        }
    }


}