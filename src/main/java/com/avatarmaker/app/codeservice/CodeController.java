package com.avatarmaker.app.codeservice;

import com.avatarmaker.app.converter.CodeConverter;
import com.avatarmaker.app.CodeLoader.CodeHandlerService;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.lang.Object;

@RestController
public class CodeController {
    @GetMapping("/")
    @CrossOrigin(origins = {"http://localhost:3000", "https://avatarmaker.netlify.com", "https://avatarmaker.netlify.app"})
    @ResponseBody
    public String status() {
        return "Code converter is available to use";
    }

    @RequestMapping(value="/convert/encode", method={RequestMethod.POST})
    @CrossOrigin(origins = {"http://localhost:3000", "https://avatarmaker.netlify.com", "https://avatarmaker.netlify.app"})
    @ResponseBody
    public Map<Object, Object> encode(@RequestBody Argument arg) {
        HashMap<Object, Object> data = new HashMap<>();
        try {
            List<String> rawCodes = new ArrayList<>();
            for (int value: arg.getData()) {
                rawCodes.add(String.valueOf(value));
            } 
            data.put("status", 200);
            data.put("data", CodeConverter.encode(rawCodes));
            return data;
        } catch (Exception e) {
            data.put("status", 500);
            return data;
        }
    }

    @GetMapping("/convert/decode")
    @CrossOrigin(origins = {"http://localhost:3000", "https://avatarmaker.netlify.com", "https://avatarmaker.netlify.app"})
    @ResponseBody
    public Map<Object, Object> decode(@RequestParam(name = "code", required = true) String code) {
        HashMap<Object, Object> data = new HashMap<>();
        try {
            CodeHandlerService codeHandlerService = new CodeHandlerService();
            codeHandlerService.handle(code);
            List<String> list = CodeConverter.decode(code);
            int[] convertedCodes = new int[list.size()];
            for (int index = 0; index < list.size(); index++) {
                convertedCodes[index] = Integer.parseInt(list.get(index));
            }
            data.put("status", 200);
            data.put("data", convertedCodes);
            return data;
        } catch (Exception e) {
            data.put("status", 500);
            data.put("message", e.getMessage());
           return data;
        }
        
    }
}