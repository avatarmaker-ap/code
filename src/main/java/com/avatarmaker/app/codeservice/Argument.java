package com.avatarmaker.app.codeservice;

public class Argument {
    private int[] data;

    public Argument() {

    }

    public Argument(int[] data) {
        this.data = data;
    }
    
    public int[] getData() {
        return this.data;
    }
}