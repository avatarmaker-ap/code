package com.avatarmaker.app.converter;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class NumberExceptionTest {
    @Test
    public void throwException() {
        try {
            throw new NumberException("This is fail");
        } catch(Exception e) {
            assertEquals("This is fail", e.getMessage());
        }
    }
}
