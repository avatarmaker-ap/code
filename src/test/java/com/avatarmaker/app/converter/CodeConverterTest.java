package com.avatarmaker.app.converter;

import static org.junit.Assert.assertEquals;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;


public class CodeConverterTest {
    @Test
    public void simpleDecodeTest() {
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("371");
        codes.add("80");
        codes.add("686");
        codes.add("289");
        assertEquals(codes, CodeConverter.decode("ab28j281"));

        codes.clear();
        codes.add("371");
        codes.add("80");
        codes.add("686");
        assertEquals(codes, CodeConverter.decode("ab28j2"));
    }

    @Test
    public void edgeDecodeTest() {
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("7");
        codes.add("0");
        codes.add("36");
        codes.add("100");
        assertEquals(codes, CodeConverter.decode("0700102s"));

        codes.clear();
        codes.add("7");
        codes.add("0");
        assertEquals(codes, CodeConverter.decode("0700"));
    }

    @Test
    public void simpleEncodeTest() {
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("98");
        codes.add("1295");
        codes.add("300");
        codes.add("666");
        assertEquals("2qzz8cii", CodeConverter.encode(codes));

        codes.clear();
        codes.add("1295");
        assertEquals("zz", CodeConverter.encode(codes));
    }

    @Test
    public void edgeEncodeTest() {
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("00000");
        codes.add("0");
        codes.add("8");
        codes.add("666");
        assertEquals("000008ii", CodeConverter.encode(codes));
        
        codes.clear();
        codes.add("0");
        codes.add("666");
        assertEquals("00ii", CodeConverter.encode(codes));
        assertEquals(codes, CodeConverter.decode(CodeConverter.encode(codes)));
    }
}
