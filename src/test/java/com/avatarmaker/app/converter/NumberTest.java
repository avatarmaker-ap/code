package com.avatarmaker.app.converter;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.avatarmaker.app.converter.NumberException;

public class NumberTest {
    @Test
    public void downConvertTest() {
        assertEquals("10111", Number.convertTo("23", 10, 2));
        assertEquals("37", Number.convertTo("1f", 16, 8));
        assertEquals("0", Number.convertTo("00000", 13, 3));
    }

    @Test
    public void sameBaseTest() {
        assertEquals("2342001", Number.convertTo("2342001", 5, 5));
        assertEquals("ab", Number.convertTo("ab", 12, 12));
        assertEquals("58", Number.convertTo("0058", 10, 10));
        assertEquals("6600", Number.convertTo("6600", 10, 10));
        assertEquals("w4d1d4w", Number.convertTo("w4d1d4w", 36, 36));
        assertEquals("0", Number.convertTo("0000", 20, 20));
    }

    @Test
    public void upConvertTest() {
        assertEquals("16", Number.convertTo("10000", 2, 10));
        assertEquals("0", Number.convertTo("0000", 5, 10));
        assertEquals("2d", Number.convertTo("231", 4, 16));
        assertEquals("32", Number.convertTo("76", 8, 20));
    }

    @Test(expected = NumberException.class)
    public void baseIsNegativeTest() {
        Number.convertTo("6273", -7, 12);
    }

    @Test(expected = NumberException.class)
    public void baseIsMoreThanMaximumTest() {
        Number.convertTo("6273", 12, 40);
    }
}
