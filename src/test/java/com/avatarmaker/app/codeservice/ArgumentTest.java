package com.avatarmaker.app.codeservice;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ArgumentTest {
    @Test
    public void createArgumentTest() {
        int[] data = {1, 2, 3, 4};
        Argument arg = new Argument(data);
        assertEquals(data, arg.getData());

        arg = new Argument();
        assertEquals(null, arg.getData());
    }
}
