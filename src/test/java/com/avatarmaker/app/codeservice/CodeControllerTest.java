/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avatarmaker.app.codeservice;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CodeController.class)
public class CodeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void statusTest() throws Exception {
        mockMvc.perform(get("/")).andExpect(content().string(containsString("Code converter is available to use")))
                .andExpect(content().string(not(containsString("Code converter is not available to use"))));
    }

    @Test
    public void encodeTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        int[] data = { 28, 93, 19, 45 };
        Argument arg = new Argument(data);
        mockMvc.perform(post("/convert/encode/").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(arg))).andExpect(content().json("{status: 200, data: '0s2l0j19'}"));
    }

    // @Test
    // public void badRequestEncodeTest() {
    //     ObjectMapper mapper = new ObjectMapper();
    //     int[] data = {28, 93, 1992, 45};
    //     Argument arg = new Argument(data);
    //     mockMvc.perform(post("/convert/encode/")
    //             .contentType(MediaType.APPLICATION_JSON)
    //             .content(mapper.writeValueAsString(arg)))
    //             .andExpect(content().json("{status: 500}"));
    // }

    @Test
    public void decodeTest() throws Exception {
       mockMvc.perform(get("/convert/decode?code=01110304"))
                .andExpect(content().json("{status: 200, data: [1, 37, 3, 4]}"))
                .andExpect(content().json("{status: 200, data: [01, 37, 03, 04]}"));
    }

}
