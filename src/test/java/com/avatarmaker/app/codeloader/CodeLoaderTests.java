package com.avatarmaker.app;

import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
import com.avatarmaker.app.CodeLoader.CodeHandlerService;
import com.avatarmaker.app.CodeLoader.CodeHandlerException;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;

public class CodeLoaderTests {
    CodeHandlerService dummyService;

    //@BeforeEach
    //void setUp(){
    //    dummyService = new CodeHandlerService();
    //}
    @Test
    public void testChainSuccessful() {
        dummyService = new CodeHandlerService();
        try {
            dummyService.handle("AABBCCDD");
        }
        catch (CodeHandlerException e){
            fail("Failure at testChainSuccessful: an exception should not be thrown");
        }
    }

    @Test
    public void testChainSymbolFail(){
        dummyService = new CodeHandlerService();
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        try {
            dummyService.handle("*BBBBBBB");
            fail("Failure at testChainSymbolFail: dummyService.handle(code) should throw an exception");
        }
        catch (CodeHandlerException e){
            assertEquals("Failure at testChainLengthFail: Message should be Invalid symbol",
                    "Invalid symbol",e.getMessage());
        }
    }

    @Test
    public void testChainLengthFail(){
        dummyService = new CodeHandlerService();
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        try {
            dummyService.handle("BBBBBBB");
            fail("Failure at testChainLengthFail: dummyService.handle(code) should throw an exception");
        }
        catch (CodeHandlerException e){
            assertEquals("Failure at testChainLengthFail: Message should be Invalid length","Invalid length",e.getMessage());
        }
    }

}
